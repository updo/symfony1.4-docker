# Welcome to StackEdit!
# docker-nginx-phpfpm-mysql-symfony14
Everything you need to get Symfony 1.4 apps running in Docker.

# configure docker-compose file

 

 - port :
ports:
      "8000:80"
      le serveur web sera accessible sur le port 8000 
 - ton code source : ( apache)
       volumes:
             - le/repertoire/de/ton/code/source:/var/www/html/ton_application 
 - sql creation de la base de données
  volumes:
      le/fichier/sql/pour/charger/ta/base:/docker-entrypoint-initdb.d/schema.sql:ro

# How this works

- Add your project to www directory
- Run

```shell
docker-compose up
```

- Profit!

Included are the following:

- Nginx
- PHP5.3 FPM + XDebug
- Mysql 5.5

Also included a script (/bin/fix-cache-log.sh) to create cache & log dirs in the root directory, thereby avoiding permissions issues running this in OSX.

